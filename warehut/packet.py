from collections import namedtuple

Packet = namedtuple('Packet', ['event', 'data'])